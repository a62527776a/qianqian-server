define({ "api": [
  {
    "type": "get",
    "url": "/ali/signature",
    "title": "获取阿里鉴签后的URL",
    "description": "<p>具体参考阿里OpenApi各个请求参数 (https://help.aliyun.com/document_detail/69050.html?spm=a2c4g.11186623.6.657.RMiAWQ) action与参数必需对应</p>",
    "name": "getAliSignature",
    "group": "aliSignature",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "action",
            "description": "<p>Api类型</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "VideoId",
            "description": "<p>视频ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Definition",
            "description": "<p>视频流清晰度。 ...</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  code: 200,\n  data: \"http://vod.cn-shanghai.aliyuncs.com/?AccessKeyId=LTAI57mk3kLMPqsv&Action=GetVideoList&Format=\n   JSON&SignatureMethod=HMAC-SHA1&SignatureNonce=a709b04f195f12df29623e7b2a6fde59&SignatureVersion=1.0&\n   Timestamp=2018-05-28T06%3A04%3A04Z&Version=2017-03-21&Signature=y6yUYnEp2GjSXHQb94JJre3WUAc%3D\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controller/ali-core.js",
    "groupTitle": "aliSignature"
  },
  {
    "type": "delete",
    "url": "/share-text",
    "title": "删除分享文本底下的字段",
    "name": "DeleteShareText",
    "group": "shareText",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>分享文本唯一标识</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controller/share-text.js",
    "groupTitle": "shareText"
  },
  {
    "type": "get",
    "url": "/share-text",
    "title": "获取分享文本底下的字段",
    "name": "GetShareText",
    "group": "shareText",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  code: 200,\n  data: [\n    {\n        \"enable\": true,\n        \"_id\": \"5af509b39f49a71f70206808\",\n        \"title\": \"eewe\",\n        \"desc\": \"不好\",\n        \"btnText\": \"哦哦\"\n    },\n    {\n        \"enable\": false,\n        \"_id\": \"5af509b39f49a71f70206808\",\n        \"title\": \"eewe\",\n        \"desc\": \"不好\",\n        \"btnText\": \"哦哦\"\n    },\n    {\n        \"enable\": false,\n        \"_id\": \"5af509b39f49a71f70206808\",\n        \"title\": \"eewe\",\n        \"desc\": \"不好\",\n        \"btnText\": \"哦哦\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controller/share-text.js",
    "groupTitle": "shareText"
  },
  {
    "type": "get",
    "url": "/share-text/able",
    "title": "获取被激活的分享文本",
    "name": "GetShareTextByAble",
    "group": "shareText",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   code: 200,\n   data: [\n     {\n         \"enable\": true,\n         \"_id\": \"5af509b39f49a71f70206808\",\n         \"title\": \"eewe\",\n         \"desc\": \"不好\",\n         \"btnText\": \"哦哦\"\n     },\n     {\n         \"enable\": true,\n         \"_id\": \"5af509b39f49a71f70206808\",\n         \"title\": \"eewe\",\n         \"desc\": \"不好\",\n         \"btnText\": \"哦哦\"\n     }\n   ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controller/share-text.js",
    "groupTitle": "shareText"
  },
  {
    "type": "post",
    "url": "/share-text",
    "title": "创建分享文本底下的字段",
    "name": "PostShareText",
    "group": "shareText",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>分享文本标题</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>分享文本描述</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "btnText",
            "description": "<p>分享按钮文字</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "enable",
            "defaultValue": "false",
            "description": "<p>是否显示</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"data\": {\n    _id: _id,\n   title: title,\n   desc: desc,\n   btnText: btnText,\n   enable: enable\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controller/share-text.js",
    "groupTitle": "shareText"
  },
  {
    "type": "put",
    "url": "/share-text",
    "title": "更新分享文本底下的字段",
    "name": "PutShareText",
    "group": "shareText",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>分享文本唯一标识</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "title",
            "description": "<p>分享文本标题</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "desc",
            "description": "<p>分享文本描述</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "btnText",
            "description": "<p>分享按钮文字</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "enable",
            "description": "<p>是否显示</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controller/share-text.js",
    "groupTitle": "shareText"
  },
  {
    "type": "post",
    "url": "/user/signin",
    "title": "用户登录",
    "description": "<p>根据用户名密码查询主应用接口用户信息 使用主应用账户信息登录</p>",
    "name": "UserSignIn",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "username",
            "description": "<p>用户名/手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "password",
            "description": "<p>用户密码</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"data\": {\n    token: xxxxx\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controller/user.js",
    "groupTitle": "user"
  }
] });
