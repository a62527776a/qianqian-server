'use strict';

let mongoose = require('mongoose')
mongoose.connect('mongodb://127.0.0.1:27017/admin', {
  useNewUrlParser: true
})

let dateFormat = require('./util/date-format.js')

dateFormat()

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  const token = app.middleware.token
  // app.redis.on('ready', () => {
  //   console.log('ready')
  // })
  // app.redis.on('error', (e) => {
  //   console.log(e)
  // })
  let db = mongoose.connection
  db.once('open', function() {
    global.mongoose = mongoose
  });
  /**
   * 异常记录暂时记录到统计中 该API停止废弃
   */
  // 异常记录
  router.post('/api/err-record', controller.errRecord.create)

  router.post('/api/formid', controller.sendMessage.submitFormId)
  router.post('/api/sendMessage', controller.sendMessage.sendMessage)
  router.get('/api/wx/login', controller.sendMessage.wxLogin)
  router.get('/api/sendMessage/statistics', controller.sendMessage.statistics)
  // 分享文本
  router.post('/api/share-text', controller.shareText.create)
  router.get('/api/share-text', controller.shareText.find)
  router.get('/api/share-text/able', controller.shareText.findByAble)
  router.put('/api/share-text', controller.shareText.update)
  router.delete('/api/share-text', controller.shareText.deleteById)
  // router.delete('/api/share-text/more', controller.shareText.deleteByIds)

  // 用户
  router.post('/api/user/signin', controller.user.signIn)
  router.get('/api/user/signup', controller.user.signUp)

  router.get('/v3plus/activity/xrkj/info', controller.activity.v3plusActivityXrkjInfo)
  
  // 上传图片
  router.post('/api/source/update', controller.source.create)
  router.get('/api/source/update', controller.source.find)
  router.delete('/api/source/update', controller.source.deleteSource)
  router.get('/api/source/all', controller.source.getAll)
  router.post('/api/source/updateByUrls', controller.source.createByImgUrls)
  router.get('/api/source/unsplash', controller.source.unsplash)
  router.get('/api/douban', controller.source.douban)

  // 选图的标题
  router.post('/api/title', controller.title.update)
  router.get('/api/title', controller.title.get)

  // 是否展示广告
  router.post('/api/ad', controller.ad.update)
  router.get('/api/ad', controller.ad.find)

  // 优秀实例
  router.post('/api/example', controller.example.post)
  router.get('/api/example/type', controller.example.findByType)
  router.get('/api/example', controller.example.get)

  // 书签主题内容
  router.post('/api/bookmark', controller.bookmark.update)
  router.get('/api/bookmark', controller.bookmark.find)

  // 爬取https://unsplash.com/的图片作为图库
  router.get('/api/crawler', controller.crawler.get)

  router.post('/api/youtube', controller.youtube.downloadVideo)
  router.get('/api/youtube/top', controller.youtube.top)

  router.get('/', controller.youtube.index)

  // service-worker
  router.get('/service-worker.js', controller.youtube.serviceworker)
  router.get('/jd_root.txt', controller.youtube.jdRoot)


  
  router.get('/game/toplist', controller.game.findToplist)
  router.post('/game/toplist', app.middleware.verifyGameScore(), controller.game.postToplist)
  router.delete('/game/toplist', controller.game.deleteToplist)
}
