## 关于TabScroll 

###### TabScroll是一个基于Vue(2.x)和Better-Scroll开发的手势库，主要用于满足左右手势以及上下手势的切换。 由于依赖Better-Scroll，所以需要对Better-Scroll具有一定的了解。 如果还没有了解过，建议先看看Better-Scroll官方文档。

<div class="px-line"></div>

## 使用：
```
// main.js
import Vue from 'vue'
import App from './App.vue'

import tabScroll from 'tab-scroll'

Vue.use(tabScroll)

new Vue({
  render: h => h(App)
}).$mount('#app')
          </pre>
          <pre>
// App.vue
<template>
  <div class="wrapper">
    <vue-horizontal-scroll ref="vue-horizontal-scroll" @scrollEnd="horizontalScrollEnd">
      <vue-vertical-scroll :ref="'scroll-' + idx" @pullingUp="pullingUp(idx)">
        <some-item v-for="(item, idx) in items" :key="idx"><some-item>
      </vue-vertical-scroll>
      <vue-vertical-scroll :ref="'scroll-' + idx"></vue-vertical-scroll>
      <vue-vertical-scroll :ref="'scroll-' + idx"></vue-vertical-scroll>
    </vue-horizontal-scroll>
  </div>
</template>

<script>
  export default {
    name: 'App.vue',
    data () {
      return {
        pageIdx: 0,
        items: [
          do...
        ]
      }
    },
    methods: {
      /**
       * @method horizontalScrollEnd
       * @param { Number } idx 横向切换的下标
       * 横向切换结束时将触发scrollEnd事件 参数为切换到的下标
       */
      horizontalScrollEnd: function (idx) {
        this.pageIdx = idx
      },
      /**
       * @method pullingUp
       * @param { Number } idx 通过下标获取是哪个scroll触发的事件
      pullingUp: function (idx) {
        // 去获取数据
      }
    }
  }
</script>
```

## 安装 & 使用

<div class="px-line"></div>

***安装***

TabScroll依赖Better-Scroll，如果您未安装过Better-Scroll，请将Better-Scroll一并安装
```
yarn add better-scroll tab-scroll
```
如果您安装过Better-Scroll，那么请直接安装TabScroll
```
yarn add tab-scroll
```

***使用***
```
// main.js
import Vue from 'vue'
import App from './App.vue'

import tabScroll from 'tab-scroll'

Vue.use(tabScroll)

new Vue({
  render: h => h(App)
}).$mount('#app')
          </pre>
          <pre>
// App.vue
<template>
  <div class="wrapper">
    <vue-horizontal-scroll ref="vue-horizontal-scroll" @scrollEnd="horizontalScrollEnd">
      <vue-vertical-scroll :ref="'scroll-' + idx" @pullingUp="pullingUp(idx)">
        <some-item v-for="(item, idx) in items" :key="idx"><some-item>
      </vue-vertical-scroll>
      <vue-vertical-scroll :ref="'scroll-' + idx"></vue-vertical-scroll>
      <vue-vertical-scroll :ref="'scroll-' + idx"></vue-vertical-scroll>
    </vue-horizontal-scroll>
  </div>
</template>

<script>
  export default {
    name: 'App.vue',
    data () {
      return {
        pageIdx: 0,
        items: [
          do...
        ]
      }
    },
    methods: {
      /**
       * @method horizontalScrollEnd
       * @param { Number } idx 横向切换的下标
       * 横向切换结束时将触发scrollEnd事件 参数为切换到的下标
       */
      horizontalScrollEnd: function (idx) {
        this.pageIdx = idx
      },
      /**
       * @method pullingUp
       * @param { Number } idx 通过下标获取是哪个scroll触发的事件
      pullingUp: function (idx) {
        // 去获取数据
      }
    }
  }
</script>
```

<div class="px-line"></div>

## 文档
<div class="px-line"></div>

```
<vue-horizontal-scroll></vue-horizontal-scroll>
```
###### 横向滚动的主体 本质是一个swiper

##### Props

Option|Type|Default|Params|Description
--|:--:|--:|--:|--:
options|Object|{}|/|默认配置可以通过$refs['vue-horizontal-scroll'].horizontalScrollDefaultOpt查看，传入的options将merge入horizontalScrollDefaultOpt中
height|String|(screenHeight - wrapperOffsetTop) + 'px'|/|默认不传将根据屏幕高度以及BScroll的offsetTop自动计算高度 如果有自定义需求，可以传入px、rem、vh、等单位的字符串

##### Methods

Name|Param|Type|Default|Return|Description
--|:--:|--:|--:|--:|--:
goToPage|index|Number|/|/|传入下标横向滚动至哪一页
computedWidth|/|/|/|/|计算horizontalScroll宽度，会自动调用，一般情况下无需调用。仅用于tab栏为动态的情况下。比如从后端拉取菜单的情况。请务必包裹进$nextTick。因为它会去读取dom节点


##### Event

Name|Param|Type|Description
--|:--:|--:|--:|--:|--:
scrollEnd|pageX|Number|/|横向滚动停止时会向上触发scrollEnd事件 参数为横向滚动的下标
computedWidth|/|/|/|/|计算horizontalScroll宽度，会自动调用，一般情况下无需调用。仅用于tab栏为动态的情况下。比如从后端拉取菜单的情况。请务必包裹进$nextTick。因为它会去读取dom节点

```
<vue-vertical-scroll></vue-vertical-scroll>
```

###### 竖向滚动的主体 默认不开启下拉刷新，请通过options配置开启

##### Props

Option|Type|Default|Params|Description
--|:--:|--:|--:|--:
options|Object|{}|/|默认配置可以通过$refs['vue-vertical-scroll'].verticalScrollDefaultOpt中
height|String|(screenHeight - wrapperOffsetTop) + 'px'|/|默认不传将根据屏幕高度以及BScroll的offsetTop自动计算高度 如果有自定义需求，可以传入px、rem、vh、等单位的字符串
