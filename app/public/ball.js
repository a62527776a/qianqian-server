(() => {
  const { easing, physics, spring, tween, styler, listen, value, transform } = window.popmotion;
const { pipe, clampMax } = transform;

const ball = document.querySelector('.ball');
const yellow_ball = document.querySelector('.yellow_ball');
const red_ball = document.querySelector('.red_ball');
const score_area = document.querySelector('.score');
const ballStyler = styler(ball);
const ballY = value(0, (v) => ballStyler.set('y', Math.min(0, v)));
const ballScale = value(1, (v) => {
  ballStyler.set('scaleX', 1 + (1 - v));
  ballStyler.set('scaleY', v);
});
let count = 0;
let scoreMap = []
let score = 0
let acceleration = 3500
let isFalling = false;
let achievement = {
  count: 0,
  scoreMap: 0,
  score: 0
}

const ballBorder = value({
  borderColor: '',
  borderWidth: 0
}, ({ borderColor, borderWidth }) => ballStyler.set({
  boxShadow: `0 0 0 ${borderWidth}px ${borderColor}`
}));

const checkBounce = () => {
  if (!isFalling || ballY.get() < 0) return;
  
  isFalling = false;
  const impactVelocity = ballY.getVelocity();
  const compression = spring({
    to: 1,
    from: 1,
    velocity: - impactVelocity * 0.01,
    stiffness: 800
  }).pipe((s) => {
    if (s >= 1) {
      s = 1;
      compression.stop();
      
      if (impactVelocity > 20) {
        isFalling = true;
        gravity
          .set(0)
          .setVelocity(- impactVelocity * 0.5);
      }
    }
    return s;
  }).start(ballScale);
};

const checkFail = () => {
  if (ballY.get() >= 0 && ballY.getVelocity() !== 0 && red_ball.innerHTML !== 'Tap') {
    if (count !== 0) {
      achievement = {
        count: count,
        score: score,
        scoreMap: scoreMap
      }
      $('.showRings').remove()
    }
    count = 0;
    score = 0;
    scoreMap = [];
    tween({
      from: { borderWidth: 0, borderColor: 'rgb(255, 28, 104, 1)' },
      to: { borderWidth: 30, borderColor: 'rgb(255, 28, 104, 0)' }
    }).start(ballBorder);

    red_ball.innerHTML = '<a>Tap</a>';
  }
};

const gravity = physics({
  acceleration: 3500,
  restSpeed: false
}).start((v) => {
  ballY.update(v);
  checkBounce(v);
  checkFail(v);
});

const showRing = (e, ringStr) => {
  $('body').append(`<a class="showRings" style="left: ${e.touches[0].clientX}px;top: ${e.touches[0].clientY}px">${ringStr}</a>`)
} 

const computedScore = (e) => {
  let oldScore = score
  let ringStr = ''
  if (e.target === ball) {
    score += 10
    scoreMap.push('qweewq')
    ringStr = 'success!'
  } else if (e.target === yellow_ball) {
    score += 20
    scoreMap.push('65567')
    ringStr = 'cool!'
  } else {
    score += 30
    scoreMap.push('8743678')
    ringStr = 'nice!'
  }
  showRing(e, ringStr)
  tween({
    from: oldScore,
    to: score
  }).pipe(Math.round)
    .start((v) => {
    score_area.innerHTML = v + '分'
  });
}

const tweenBall =  (e) => {
  e.preventDefault();
  count++;
  computedScore(e)
  red_ball.innerHTML = `<a>${count}</a>`;
  isFalling = true;
  ballScale.stop();
  ballScale.update(1);

  gravity
    .setAcceleration((acceleration + (count * 100)))
    .set(Math.min(0, ballY.get()))
    .setVelocity(-1200);

  tween({
    from: { borderWidth: 0, borderColor: 'rgb(20, 215, 144, 1)' },
    to: { borderWidth: 30, borderColor: 'rgb(20, 215, 144, 0)' }
  }).start(ballBorder);
}
listen(ball, 'mousedown touchstart', {
  capture: false
}).start(tweenBall);

let postTopList = function () {
  let name = $('#name').val()
  if (!name) return window.alert('请填入你的大名')
  if (name.length > 8) return window.alert('允许输入8个字符串以下')
  $.ajax({
    url: '/game/toplist',
    method: 'POST',
    headers: {
      'x-csrf-token': 'token'
    },
    data: {
      name: name,
      scoreMap: JSON.stringify(achievement.scoreMap),
      score: achievement.score,
      count: achievement.count
    },
    success: function () {
      window.alert('提交成功')
      findTopList()
    }
  })
}

$('.submit').click(postTopList)

let findTopList = function () {
  $('.top-list').empty()
  $.ajax({
    url: '/game/toplist',
    success: function (res) {
      let topListHtml = ``
      for (let i = 0; i < res.data.length; i++) {
        topListHtml += `第${i + 1}名 ${res.data[i].name} 分数：${res.data[i].achievement.score} 三环击中数：${res.data[i].achievement.maxRings}<br />`
      }
      $('.top-list').append(topListHtml)
    }
  })
}

$(document).ready(() => {
  findTopList()
})

})()