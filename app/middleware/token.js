module.exports = () => {
  return async function checkToken(ctx, next) {
    await next()

    if (!ctx.request.header.authorization) {
      ctx.body = {
        code: 403,
        message: 'token无效'
      }
    }
    let verify = await ctx.service.user.verifyToken(ctx.request.header.authorization)
    if (verify.message.name === 'TokenExpiredError') {
      ctx.body = {
        code: 403,
        message: 'token过期'
      }
    }
    if (verify.message.name === 'JsonWebTokenError') {
      ctx.body = {
        code: 403,
        message: 'token无效'
      }
    }
  }
}