module.exports = (ctx) => {
  try {
    if (!ctx.request.body.name) throw new Error('Required parameter missing: name')
    if (!ctx.request.body.scoreMap) throw new Error('Required parameter missing: scoreMap')
    if (!ctx.request.body.score) throw new Error('Required parameter missing: score')
    if (!ctx.request.body.count) throw new Error('Required parameter missing: count')
    if (ctx.request.header['user-agent'].toLowerCase().indexOf('postman') > -1) throw new Error('验证失败')
    const scoreMap = JSON.parse(ctx.request.body.scoreMap)
    const score = parseInt(ctx.request.body.score)
    const count = parseInt(ctx.request.body.count)
    if (scoreMap.length !== count) throw new Error('验证失败')
    if (scoreMap.length > 75 || count > 75) throw new Error('大佬给你递茶')
    let scoreOpt = {
      'qweewq': 10,
      '65567': 20,
      '8743678': 30
    }
    let _score = 0
    for (let i = 0; i < scoreMap.length; i++) {
      _score += scoreOpt[scoreMap[i]]
    }
    if (_score !== score) throw new Error('验证失败')
  } catch (e) {
    throw new Error('验证失败')
  }
}