var multer = require('multer')

var storage = multer.diskStorage({
  // 设置上传文件路径,以后可以扩展成上传至七牛,文件服务器等等
  // Note:如果你传递的是一个函数，你负责创建文件夹，如果你传递的是一个字符串，multer会自动创建
  destination: 'app/public/uploads',
  // 获取文件MD5，重命名，添加后缀,文件重复会直接覆盖
  filename: function (req, file, cb) {
    try {
    console.log(file)
    var fileFormat =(file.originalname).split(".");
    cb(null, req.body.title + '-' + Date.now() + "." + fileFormat[fileFormat.length - 1])} catch (e) {
    console.log(e)
    }
  }
});

// 添加配置文件到muler对象。
var upload = multer({
  storage: storage,
});

module.exports = upload;
