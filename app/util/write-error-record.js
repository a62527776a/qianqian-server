const fs = require('fs')
const path = require('path')

/**
 * @method writeErrorRecord 写入错误日志
 * @param { Object } body 要写入的数据
 * @var { Date } today 日期 日志根据日期分类 一天的日期放在一个日志文件中
 * @var { String } path 路径 日志文件放置在一个路径下
 */
let writeErrorRecord = async (body) => {
  try {
    if (!body.isApp) throw new Error('Required parameter missing')
    const today = new Date().format('yyyy-MM-dd')
    const path = `${__dirname}/../../static/err-record-text/${today}.text`
    body.createTime = new Date()
    // 将内容追加进日志文件中 读取时采用将第一个逗号去除并JSON.parse([${data}])的方式
    fs.appendFile(path, ',' + JSON.stringify(body) + '\n')
  } catch (e) {
    throw new Error(e)
  }
}

module.exports = writeErrorRecord;
