'use strict';

const Service = require('egg').Service;
let Source = require('../model/source')

class SourceService extends Service {
  async postSource () {
    let source = await new Source({
      title: this.ctx.req.body.title,
      url: this.ctx.req.file.path
    }).save()
    return source
  }
}

module.exports = SourceService;
