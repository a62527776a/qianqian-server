const Service = require('egg').Service
const crypto = require('crypto')

class UserService extends Service {
  /**
   * @method getUserStatus 获取用户状态 从主应用接口获取 返回主引用接口数据
   */
  async getUserStatus () {
    const body = this.ctx.request.body
    let md5 = crypto.createHash("md5").update(body.password)
    let md5Password = md5.digest('hex')
    const userResult = await this.app.curl('http://web.rr.tv/user/mobileLogin', {
      method: 'POST',
      data: `clientType=web&clientVersion=3.5.0&mobile=${body.mobile}&password=${md5Password}`,
      headers: {
        'clientType': 'web',
        clientVersion: '3.5.0',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    })
    return JSON.parse(new Buffer(userResult.data).toString())
  }
  /**
   * @method signJWT token签名
   * @param { Number } user_id 用户名ID
   */
  async signJWT (user_id) {
    const token = await this.app.jwt.sign({
      user_id: user_id
    }, this.app.config.jwt.secret, {
      expiresIn: '3d'
    })
    return token
  }
  async verifyToken (token) {
    const verifyToken = await this.app.jwt.verify(token, this.app.config.jwt.secret)
    return verifyToken
  }
  // 刷新token
  // @param token
  async refreshToken (token, user_id) {
    // 删除原token
    let result = await this.app.redis.get(token)
    let delResult = await this.app.redis.del(token)
    result = await this.app.redis.get(token)
    // 创建新token
    let newToken = await this.ctx.service.user.signJWT(user_id)
    await this.ctx.service.user.saveToken(newToken, user_id)
    return newToken
  }
  /**
   * 保存token
   */
  async saveToken (token, user_id) {
    // 超时时间 3天
    const saveResult = await this.app.redis.set(token, user_id, 'EX', (60 * 60 * 24 * 3))
  }
  async delToken () {
    await this.app.redis.del(this.ctx.request.header.authorization)
  }
}

module.exports = UserService;
