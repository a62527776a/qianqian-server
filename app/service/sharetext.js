'use strict';

const Service = require('egg').Service;
const ShareText = require('../model/sharetext')

class ShareTextService extends Service {
  async findShareTextByAble () {
    let shareText = await ShareText.find({'enable': true}, '-__v -_id -enable')
    return shareText
  }
  async findShareText () {
    let shareText = await ShareText.find({}, '-__v')
      .sort({'enable': -1})
    return shareText
  }
  async postShareText () {
    let shareText = await new ShareText(this.ctx.request.body).save()
    return shareText
  }
  async update() {
    let shareText = await ShareText.findByIdAndUpdate(this.ctx.request.body._id, this.ctx.request.body)
    return shareText
  }
  async delete() {  
    let shareText = await ShareText.findByIdAndRemove(this.ctx.request.body._id)
    return shareText
  }
  // async deleteByIds() {
  //   let shareText = await ShareText.findByIdAndRemove(this.ctx.request.body._id)
  //   return shareText
  // }
}

module.exports = ShareTextService;
