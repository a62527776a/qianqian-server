'use strict';

const Service = require('egg').Service
const Video = require('../model/youtube-video')
const Parserecord = require('../model/parse-record')

class YoutubeService extends Service {
  /**
   * @method resVideoByDB 从数据库返回数据
   * 如果数据存在则返回数据并增加一次解析热度
   * @return { Object | false }
   */
  async resVideoByDB () {
    let url = this.ctx.request.body.url
    let videoResult = await Video.findOne({id: url})
    return videoResult ? videoResult.body : false
  }
  /**
   * @method parseRecord 处理解析记录
   * 根据IP以及URL查看DB中是否有该条记录
   * 如果没有 则存入数据
   */
  async parseRecord () {
    let body = {
      id: this.ctx.request.body.url,
      ip: this.ctx.request.ip
    }
    let record = await Parserecord.find(body)
    if (!record || record.length === 0) {
      new Parserecord({
        id: this.ctx.request.body.url,
        createTime: Date.now(),
        ip: this.ctx.request.ip
      }).save()
    }
  }
}

module.exports = YoutubeService;
