'use strict';

const Service = require('egg').Service
const YuepiandakaSeason = require('../model/h5/yuepiandaka/yuepian-daka-season')
const YuepiandakaPeriods = require('../model/h5/yuepiandaka/yuepian-daka-periods')

class YuepiandakaService extends Service {
  async postSeason () {
    let season = await new YuepiandakaSeason(this.ctx.request.body).save()
    let periods = await YuepiandakaPeriods.find({_id: this.ctx.request.body.periods})
    season.periods = periods
    return season
  }
  async postPeriods () {
    const periods = await new YuepiandakaPeriods(this.ctx.request.body).save()
    return periods
  }
  async getSeason () {
    const season = await YuepiandakaSeason.find({}, '-__v')
      .populate({ path: 'periods'})
    return season
  }
  async getPeriods () {
    const periods = await YuepiandakaPeriods.find({}, '-__v')
    return periods
  }
  async deleteSeason () {
    const season = await YuepiandakaSeason.findByIdAndRemove(this.ctx.request.body._id)
    return season
  }
  async deletePeriods () {
    const periods = await YuepiandakaPeriods.findByIdAndRemove(this.ctx.request.body._id)
    return periods
  }
}

module.exports = YuepiandakaService;
