// 热门资源组
const mongoose = require('mongoose')

const HotSourceGroup = mongoose.model('HotSourceGroup', new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  group: [{
    type: Schema.Types.ObjectId,
    ref: 'source'
  }]
}))

module.exports = HotSourceGroup