const mongoose = require('mongoose')

const ShareText = mongoose.model('ShareText', new mongoose.Schema({
  title: {
    type: String
  },
  desc: {
    type: String
  },
  btnText: {
    type: String
  },
  enable: {
    type: Boolean,
    default: false
  }
}))

module.exports = ShareText