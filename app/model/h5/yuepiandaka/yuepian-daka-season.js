const mongoose = require('mongoose')

const YuepianDakaSeason = mongoose.model('YuepianDakaSeason', new mongoose.Schema({
  seasonId: {
    type: String,
    required: true
  },
  seasonTitle: {
    type: String,
    required: true
  },
  seasonDesc: {
    type: String,
    required: true
  },
  thumbUrl: {
    type: String,
    required: true
  },
  periods: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'YuepianDakaPeriods'
  }
}))

module.exports = YuepianDakaSeason