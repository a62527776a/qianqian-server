const mongoose = require('mongoose')

const YuepianDakaPeriods = mongoose.model('YuepianDakaPeriods', new mongoose.Schema({
  periodsIdx: {
    type: String
  },
  periodsName: {
    type: String
  }
}))

module.exports = YuepianDakaPeriods