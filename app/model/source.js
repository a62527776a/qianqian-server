const mongoose = require('mongoose')

const Source = mongoose.model('Source', new mongoose.Schema({
  title: {
    type: String
  },
  url: {
    type: String
  },
  name: {
    type: String
  },
  type: {
    type: String
  },
  color: {
    type: String,
    default: '#EEE'
  },
  height: {
    type: Number,
    default: 0
  },
  width: {
    type: Number,
    default: 0
  }
}))

module.exports = Source