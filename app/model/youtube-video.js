const mongoose = require('mongoose')

const Video = mongoose.model('Video', new mongoose.Schema({
  id: {
    type: String
  },
  body: {
    type: Object
  },
  popularity: {
    type: Number,
    default: 0
  }
}))

module.exports = Video