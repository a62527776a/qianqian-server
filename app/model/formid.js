const mongoose = require('mongoose')

const FormIdModel = mongoose.model('formId', new mongoose.Schema({
  formId: {
    type: String,
    required: true
  },
  openId: {
    type: String,
    required: true
  },
  createTime: {
    type: Date,
    default: Date.now
  }
}))

module.exports = FormIdModel