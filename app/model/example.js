const mongoose = require('mongoose')

const Example = mongoose.model('Example', new mongoose.Schema({
  type: {
    type: String,
    require: true
  },
  url: {
    type: String
  }
}))

module.exports = Example