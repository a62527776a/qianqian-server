const mongoose = require('mongoose')

const GameTopList = mongoose.model('GameTopList', new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  achievement: {
    type: Object,
    required: true,
    default: {
      count: 0,
      score: 0,
      maxRings: 0
    }
  }
}))

module.exports = GameTopList