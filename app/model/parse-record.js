const mongoose = require('mongoose')

/**
 * 解析记录表 作为热度的查询依据
 * @param id 视频ID 目前以youtube地址作为id
 * @param createTime 解析时间
 * @param ip 查询时对单IP多次增加视频热度做限制 单视频仅允许增加视频热度一次 不影响解析
 */

const Parserecord = mongoose.model('Parserecord', new mongoose.Schema({
  id: {
    type: String
  },
  createTime: {
    type: Date,
    default: Date.now
  },
  ip: {
    type: String
  }
}))

module.exports = Parserecord