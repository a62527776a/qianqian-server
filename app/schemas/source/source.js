module.exports = {
  title: {
    type: 'string',
    required: true
  },
  source: {
    type: 'string',
    required: true
  }
}