module.exports = {
  title: {
    type: 'string',
    required: true
  },
  desc: {
    type: 'string',
    required: true
  },
  btnText: {
    type: 'string',
    required: true
  },
  enable: {
    type: 'boolean'
  }
}