const Controller = require('egg').Controller

let bookmark = {
  content: '不知为什么\n只要有你在我身边\n我的心便不再惶惶不安\n',
  backgroundImage: 'https://img.dscsdoj.top/egg-multipart-production/1549126035573.5876.png'
}

class BookmarkController extends Controller {
  async update () {
    bookmark = this.ctx.request.body
    this.ctx.body = {
      code: 200,
      data: bookmark
    }
  }
  async find () {
    this.ctx.body = {
      code: 200,
      data: bookmark
    }
  }
}

module.exports = BookmarkController