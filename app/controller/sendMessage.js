const Controller = require('egg').Controller
const FormIdModel = require('../model/formid')
const fetch = require('node-fetch')

class MessageController extends Controller {
  async submitFormId () {
    await new FormIdModel({
      formId: this.ctx.request.body.formId,
      openId: this.ctx.request.body.openId
    }).save()
    this.ctx.body = {
      code: 200
    }
  }

  async wxLogin () {
    let appId = 'wx93630b4188a12907'
    let appSecret = '5ae397345c04b3b46e2be970d2ec96ea'
    let res = await fetch(`https://api.weixin.qq.com/sns/jscode2session?appid=${appId}&secret=${appSecret}&js_code=${this.ctx.request.query.code}&grant_type=authorization_code`)
    res = await res.json()
    this.ctx.body = {
      code: 200,
      data: res.openid
    }
  }

  async statistics () {
    let formIds = await FormIdModel.find().distinct('openId')
    this.ctx.body = {
      code: 200,
      message: `预期发送${formIds.length}条模板消息`
    }
  }

  async sendMessage () {
    let appId = 'wx93630b4188a12907'
    let appSecret = '5ae397345c04b3b46e2be970d2ec96ea'
    let templateId = 'nI7hZvhqIAam4Ign0E42cpVNMPHcganKecJ0s5dcoLc'
    let res = await fetch(`https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=${appId}&secret=${appSecret}`)
    res = await res.json()
    let formIds = await FormIdModel.find().distinct('openId')
    formIds.map(async item => {
      let _formId = await FormIdModel.findOne({openId: item})
      let body = {
        access_token: res.access_token,
        touser: _formId.openId,
        template_id: templateId,
        form_id: _formId.formId,
        data: this.ctx.request.body,
        page: 'pages/index/main'
      }
      let _res = await fetch(`https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=${res.access_token}`, {
        method: 'post',
        body:    JSON.stringify(body),
        headers: { 'Content-Type': 'application/json' }
      })
      _res = await _res.json()
      console.log(_res, item)
      FormIdModel.deleteOne({
        formId: _formId.formId
      }).exec()
    })
    this.ctx.body = {
      code: 200,
      data: 'success'
    }
  }
}

module.exports = MessageController