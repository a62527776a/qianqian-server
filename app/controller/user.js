const Controller = require('egg').Controller
const UserSignSchema = require('../schemas/user/signin')

class UserController extends Controller {
  /**
   * @api {post} /user/signin 用户登录
   * @apiDescription 根据用户名密码查询主应用接口用户信息
   * 使用主应用账户信息登录
   * @apiName UserSignIn
   * @apiGroup user
   *
   * @apiParam {String} username 用户名/手机号
   * @apiParam {String} password 用户密码
   * 
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *       "code": 200,
   *       "data": {
   *         token: xxxxx
   *       }
   *     }
   *
   */
  async signIn () {
    try {
      const verify = await this.ctx.verify(UserSignSchema, 'body')
      const userResult = await this.ctx.service.user.getUserStatus()
      const token = this.ctx.request.header.authorization
      if (!userResult.data.user) return this.ctx.body = userResult
      // 存在Authorization字段的话 验证token
      if (token) {
        const verifyToken = await this.ctx.service.user.verifyToken(token)
        // 如果token验证存在 更新token
        if (verifyToken.user_id) {
          const tokenResult = await this.ctx.service.user.refreshToken(token, verifyToken.user_id)
          return this.ctx.body = {
            code: 200,
            data: {
              tokenResult
            }
          }
        }
      }
      // 不存在或token无效等情况直接生成并保存
      const tokenResult = await this.ctx.service.user.signJWT(userResult.data.user.id)
      const saveResult = await this.ctx.service.user.saveToken(tokenResult, userResult.data.user.id)
      this.ctx.body = {
        code: 200,
        data: {
          'token': tokenResult
        }
      }
    } catch (e) {
      console.log(e)
    }
  }
  /**
   * @api {get} /user/signup 用户登出
   * @apiDescription 删除redis token
   * @apiName UserSignUp
   * @apiGroup user
   * 
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *       "code": 200
   *     }
   *
   */
  async signUp () {
    await this.ctx.service.user.delToken()
    this.ctx.body = {
      code: 200
    }
  }
}

module.exports = UserController