'use strict';

const Controller = require('egg').Controller;
const GameTopList = require('../model/gametoplist')
const verifyGameScore = require('../util/verifyGameScore')

class GameController extends Controller {
  async findToplist () {
    try {
      let gameTopList = await GameTopList.find().sort({'achievement.score' : -1}).limit(10)
      this.ctx.body = {
        code: 200,
        data: gameTopList
      }
    } catch (e) {
      this.ctx.body = {
        code: 404,
        msg: e.message
      }
    }
  }
  async postToplist () {
    try {
      verifyGameScore(this.ctx)
      let maxRings = 0
      const scoreMap = JSON.parse(this.ctx.request.body.scoreMap)
      for (let i = 0; i < scoreMap.length; i++) {
        if (scoreMap[i] === '8743678') maxRings++
      }
      await new GameTopList({
        name: this.ctx.helper.escape(this.ctx.request.body.name),
        achievement: {
          count: parseInt(this.ctx.request.body.count),
          score: parseInt(this.ctx.request.body.score),
          maxRings: maxRings
        }
      }).save()
      this.ctx.body = {
        code: 200,
        message: 'success'
      }
    } catch (e) {
      console.log(e)
    }
  }
  async deleteToplist () {
    await GameTopList.remove()
  }
}

module.exports = GameController;
