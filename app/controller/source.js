const Controller = require('egg').Controller
const fs = require('fs')
const request = require('request')
const Path = require('path')
let Source = require('../model/source')

// 资源相关
class SourceController extends Controller {
  async create () {
    try {
      const fileStream = await this.ctx.getFileStream()
      const name = `egg-multipart-${process.env.NODE_ENV}/` + Path.basename(fileStream.filename);
      let result
      result = await this.ctx.oss.put(name, fileStream)
      await new Source({
        title: fileStream.fields.title,
        url: 'https://img.dscsdoj.top/' + result.name,
        name: name
      }).save()
      this.ctx.body = {
        code: 200
      }
    } catch (e) {
      console.log(e)
    }
    // let saveResult = await new Source({
    //   title: this.ctx.req.body.title,
    //   url: this.ctx.req.file.path.replace('app\\', '')
    // }).save()
    // console.log(saveResult)
    // this.ctx.body = {
    //   code: 200,
    //   data: {
    //     path: this.ctx.req.file.path
    //   }
    // }
  }
  async createByImgUrls () {
    let urlToFileStream = (url, color = '#EEE', name, type, width, height) => {
      return new Promise((resolve, reject) => {
        let path = Path.join(__dirname, '../public/uploads/', name)
        request(url).pipe(fs.createWriteStream(path))
          .on('close', async () => {
            let fileStream = fs.createReadStream(path)
            await this.ctx.oss.put(`egg-multipart-${process.env.NODE_ENV}/` + name, fileStream)
            await new Source({
              title: name,
              url: 'https://img.dscsdoj.top/' + `egg-multipart-${process.env.NODE_ENV}/` + name,
              name: name,
              type: type,
              color: color,
              width: width,
              height: height
            }).save()
            resolve()
          })
      })
    }
    let urls = this.ctx.request.body.urls
    let type = this.ctx.request.body.type
    for (let i = 0; i < urls.length; i++) {
      let name = Date.now() + Math.random() + i + '.png'
      await urlToFileStream(urls[i].url, urls[i].color, name, type, urls[i].width, urls[i].height)
    }
    this.ctx.body = {
      code: 200,
      message: '图片上传成功'
    }
  }
  async find () {
    let limit = ~~this.ctx.query.limit || 6
    let page = ~~this.ctx.query.page || 1
    let sources = await Source.find({})
      .sort({'_id': -1})
      .skip((page - 1) * limit)
      .limit(limit)
    this.ctx.body = {
      code: 200,
      data: sources
    }
  }
  async getAll () {
    let sources = await Source.find({}).sort({'_id': -1})
    this.ctx.body = {
      code: 200,
      data: sources
    }
  }
  async deleteSource () {
    try {
      await Source.findByIdAndRemove(this.ctx.request.body.id)
      await this.ctx.oss.delete(this.ctx.request.body.name)
      this.ctx.body = {
        code: 200,
        msg: 'success'
      }
    } catch (e) {
      this.ctx.body = {
        code: 500,
        msg: '数据删除失败'
      }
    }
  }
  async unsplash () {
    let key = this.ctx.query.type || 'love'
    let size = this.ctx.query.size || '6'
    let page = this.ctx.query.page || '1'
    try {
      let result = await this.app.curl(`https://unsplash.com/napi/search/photos?query=${key}&xp=&per_page=${size}&page=${page}`, {
        dataType: 'json'
      })
      this.ctx.body = {
        code: 200,
        data: result.data
      }
    } catch (e) {
      console.log(e)
    }
  }
  async douban () {
    let key = encodeURI(this.ctx.query.key) || ''
    let size = this.ctx.query.size || 10
    let page = this.ctx.query.page || 1
    let result = await this.app.curl(`https://api.douban.com/v2/movie/search?tag=${key}&start=${(page - 1) * size}&count=${size}`, {
      dataType: 'json'
    })
    this.ctx.body = {
      code: 200,
      data: result.data
    }
  }
}

module.exports = SourceController
