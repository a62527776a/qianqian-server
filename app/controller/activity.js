const Controller = require('egg').Controller;

class ActivityController extends Controller {
  async v3plusActivityXrkjInfo() {
    this.ctx.body = {
      requestId: 'f29d668d6e2140eca4a344b284b2695d',
      msg: '',
      code: '0000',
      data: {
        info: {
          id: 3,
          createTime: 1527749693000,
          updateTime: 1527752754000,
          createTimeStr: null,
          userId: 5706774,
          lv1: 4,
          lv2: 4,
          lv3: 2,
          lv4: 1,
          lv5: 1,
          time1: 265,
          time2: 5,
          time3: 50,
          time4: 250,
          time5: 200,
        },
      },
    };
  }
}

module.exports = ActivityController;
