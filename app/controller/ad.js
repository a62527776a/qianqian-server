const Controller = require('egg').Controller

let showAd = false

class AdController extends Controller {
  async update () {
    showAd = !showAd
    this.ctx.body = {
      code: 200,
      msg: showAd
    }
  }
  async find () {
    this.ctx.body = {
      code: 200,
      msg: showAd
    }
  }
}

module.exports = AdController