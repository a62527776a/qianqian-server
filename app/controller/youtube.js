const Controller = require('egg').Controller
const fs = require('fs')
const Path = require('path')
const Video = require('../model/youtube-video')
const Parserecord = require('../model/parse-record')
const getVideoInfo = require('../util/youtube-dl')

class YoutubeController extends Controller {
  async downloadVideo () {
    try {
      let url = this.ctx.request.body.url
      if (!url) throw new Error('参数异常')
      // 先从DB中找
      let videoResult = await this.ctx.service.youtube.resVideoByDB()
      if (videoResult) {
        this.ctx.body = {
          code: 200,
          data: videoResult
        }
        this.ctx.service.youtube.parseRecord()
        return
      }
      try {
        // 没有 再请求网络
        let videoInfo = await getVideoInfo(url)
        this.ctx.body = {
          code: 200,
          data: videoInfo
        }
        await new Video({
          id: url,
          body: videoInfo,
          popularity: 0
        }).save()
        this.ctx.service.youtube.parseRecord()
      } catch (e) {
        this.ctx.body = {
          code: 500,
          data: e.message
        }
      }
    } catch (e) {
      this.ctx.body = {
        code: 500,
        data: e.message
      }
    }
  }
  /**
   * @method getSubtitle 获取字幕
   * 
   */
  async getSubtitle () {

  }
  async index () {
    let htmlPath = Path.join(__dirname, '../public/youtube-parse/index.html')
    let html = fs.readFileSync(htmlPath)
    this.ctx.set('Content-Type', 'text/html; charset=utf-8');
    this.ctx.body = html
  }
  async jdRoot () {
    let htmlPath = Path.join(__dirname, '../public/jd_root.txt')
    let html = fs.readFileSync(htmlPath)
    this.ctx.body = html
  }
  async serviceworker () {
    let jsPath = Path.join(__dirname, '../../service-worker.js')
    let html = fs.readFileSync(jsPath)
    this.ctx.set('Content-Type', 'text/javascript; charset=utf-8');
    this.ctx.body = html
  }
  async top () {
    let videos = await Video.find().sort({popularity: -1}).limit(10)
    this.ctx.body = {
      code: 200,
      data: videos
    }
  }
}

module.exports = YoutubeController
