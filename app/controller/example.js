const Controller = require('egg').Controller
let Example = require('../model/example')
const Path = require('path')

class ExampleController extends Controller {
  async post () {
    try {
      const fileStream = await this.ctx.getFileStream()
      const name = `egg-multipart-${process.env.NODE_ENV}/` + Path.basename(fileStream.filename);
      let result
      result = await this.ctx.oss.put(name, fileStream)
      await new Example({
        type: this.ctx.query.type,
        url: 'https://img.dscsdoj.top/' + result.name,
      }).save()
      this.ctx.body = {
        code: 200
      }
    } catch (e) {
      console.log(e)
    }
  }
  async get () {
    let example = await Example.find()
    this.ctx.body = {
      code: 200,
      data: example
    }
  }
  async findByType () {
    try {
      let example = await Example.find({
        type: this.ctx.query.type
      }).limit(3).sort({'_id' : -1})
      this.ctx.body = {
        code: 200,
        data: example
      }
    } catch (e) {
      console.log(e)
    }
  }
}

module.exports = ExampleController