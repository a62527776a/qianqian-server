const Controller = require('egg').Controller

let title = ''

class TitleController extends Controller {
  async update () {
    title = this.ctx.request.body.title
    this.ctx.body = {
      code: 200,
      data: title
    }
  }

  async get () {
    this.ctx.body = {
      code: 200,
      data: title
    }
  }
}

module.exports = TitleController
