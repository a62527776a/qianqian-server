'use strict';
const writeErrorRecord = require('../util/write-error-record')

const Controller = require('egg').Controller;
/**
 * @constructor ErrorRecordController 对前端错误进行记录
 * @var { Object } userAgent 浏览器user-agent 包括手机系统 系统版本 品牌 型号 浏览器内核版本等
 * @param { String } url 发生错误时的前端路由
 * @param { Object | false } isApp 是否为app 是的话返回native信息 包括客户端版本 设备ID 以及客户端类型 iOS / IPAD_HD / Android
 * @param { String } api 当前端返回非未登录的API异常时 记录下错误接口以及返回msg msg 记录到content字段中
 * @param { String } content 前端返回非未登录的API异常时的msg 或 手动填写的错误内容
 */
class ErrorRecordController extends Controller {
  async create() {
    if (!this.ctx.request.body.url) throw new Error('Required parameter missing: url')
    if (this.ctx.request.body.isApp === undefined) throw new Error('Required parameter missing: isApp')
    let body = JSON.parse(JSON.stringify(this.ctx.request.body))
    body.userAgent = this.ctx.header['user-agent']
    this.ctx.body = {
      code: '0000'
    }
    writeErrorRecord(body)
  }
}

module.exports = ErrorRecordController;
