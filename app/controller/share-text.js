const Controller = require('egg').Controller
const ShareText = require('../model/sharetext')
const shareSchema = require('../schemas/sharetext/sharetext')
const config = require('../../config/config')

class ShareTextController extends Controller {
  /**
   * @api {post} /share-text 创建分享文本底下的字段
   * @apiName PostShareText
   * @apiGroup shareText
   *
   * @apiParam {String} title 分享文本标题
   * @apiParam {String} desc 分享文本描述
   * @apiParam {String} btnText 分享按钮文字
   * @apiParam {Boolean} [enable=false] 是否显示
   * 
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *       "code": 200,
   *       "data": {
   *         _id: _id,
   *        title: title,
   *        desc: desc,
   *        btnText: btnText,
   *        enable: enable
   *       }
   *     }
   *
   */
  async create () {
    const verify = await this.ctx.verify(shareSchema, 'body')
    // 验证不通过 返回验证结果
    if (this.ctx.request.body !== verify) return this.ctx.body = verify
    let shareText = await this.ctx.service.sharetext.postShareText()
    this.ctx.body = {
      code: 200,
      data: shareText
    }
  }
  /**
   * @api {get} /share-text 获取分享文本底下的字段
   * @apiName GetShareText
   * @apiGroup shareText
   * 
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *       code: 200,
   *       data: [
   *         {
   *             "enable": true,
   *             "_id": "5af509b39f49a71f70206808",
   *             "title": "eewe",
   *             "desc": "不好",
   *             "btnText": "哦哦"
   *         },
   *         {
   *             "enable": false,
   *             "_id": "5af509b39f49a71f70206808",
   *             "title": "eewe",
   *             "desc": "不好",
   *             "btnText": "哦哦"
   *         },
   *         {
   *             "enable": false,
   *             "_id": "5af509b39f49a71f70206808",
   *             "title": "eewe",
   *             "desc": "不好",
   *             "btnText": "哦哦"
   *         }
   *       ]
   *     }
   *
   */
  async find () {
    let body = await this.ctx.service.sharetext.findShareText()
    this.ctx.body = {
      code: 200,
      data: body
    }
  }
  /**
   * @api {get} /share-text/able 获取被激活的分享文本
   * @apiName GetShareTextByAble
   * @apiGroup shareText
   * 
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *        code: 200,
   *        data: [
   *          {
   *              "enable": true,
   *              "_id": "5af509b39f49a71f70206808",
   *              "title": "eewe",
   *              "desc": "不好",
   *              "btnText": "哦哦"
   *          },
   *          {
   *              "enable": true,
   *              "_id": "5af509b39f49a71f70206808",
   *              "title": "eewe",
   *              "desc": "不好",
   *              "btnText": "哦哦"
   *          }
   *        ]
   *     }
   *
   */
  async findByAble () {
    let body = await this.ctx.service.sharetext.findShareTextByAble()
    this.ctx.body = {
      code: 200,
      data: body
    }
  }
  /**
   * @api {put} /share-text 更新分享文本底下的字段
   * @apiName PutShareText
   * @apiGroup shareText
   *
   * @apiParam {String} _id 分享文本唯一标识
   * @apiParam {String} [title] 分享文本标题
   * @apiParam {String} [desc] 分享文本描述
   * @apiParam {String} [btnText] 分享按钮文字
   * @apiParam {Boolean} [enable] 是否显示
   * 
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *       "code": 200
   *     }
   */
  async update () {
    const verify = await this.ctx.verify({
      _id: {
        type: 'string',
        required: true
      }
    }, 'body')
    if (this.ctx.request.body !== verify) return this.ctx.body = verify
    let updateResult = await this.ctx.service.sharetext.update()
    this.ctx.body = {
      code: 200
    }
  }
  /**
   * @api {delete} /share-text 删除分享文本底下的字段
   * @apiName DeleteShareText
   * @apiGroup shareText
   *
   * @apiParam {String} _id 分享文本唯一标识
   * 
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *       "code": 200
   *     }
   */
  async deleteById () {
    const verify = await this.ctx.verify({
      _id: {
        type: 'string',
        required: true
      }
    }, 'body')
    if (this.ctx.request.body !== verify) return this.ctx.body = verify
    let deleteResult = await this.ctx.service.sharetext.delete()
    this.ctx.body = {
      code: 200
    }
  }
}

module.exports = ShareTextController