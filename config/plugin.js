'use strict';

// had enabled by egg
// exports.static = true;
exports.cors = {
  enable: true,
  package: 'egg-cors',
};

exports.validator = {
  enable: true,
  package: 'egg-y-validator'
};

exports.jwt = {
  enable: true,
  package: "egg-jwt"
};

exports.oss = {
  enable: true,
  package: 'egg-oss',
};

process.env.NODE_ENV === 'production' ? exports.alinode = {
  enable: true,
  package: 'egg-alinode'
} : {};

// exports.redis = {
//   enable: true,
//   package: 'egg-redis',
// };