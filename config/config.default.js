'use strict';
const conf = require('./config')

module.exports = appInfo => {
  const config = exports = {};
  config.multipart = {
    mode: 'file'
  }
  config.proxy = true
  config.ipHeaders = 'x-forwarded-for'
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1524281900472_7495';

  // add your config here
  config.middleware = [];

  // set csrf key
  config.security = {
    csrf: {
      enable: false
    }
  }

  config.cors = {
    enable: true,
    package: 'egg-cors',
    origin: process.env.NODE_ENV === 'development' ? '*' : 'api-dscsdoj.top',
    domainWhiteList: ['http://localhost:7001', 'http://localhost:8080'],
    allowMethods: 'GET,POST,DELETE'
  }

  config.aliKey = conf.aliKey
  config.qiniuKey = conf.qiniuKey
  config.onerror = {
    all (err, ctx) {
      ctx.body = {
        code: 500,
        message: err.message
      }
      ctx.status = 500
    }
  }

  config.validator = {
    open: async ctx => 'zh-CN',
    // or
    // open: 'zh-CN',  它表示开启的语言
    languages: {
      'zh-CN': {
        required: '%s 必填'
      }
    },
    async formatter(ctx, error) {
      ctx.type = 'json';
      ctx.status = 400;
      ctx.body = {
        code: 400,
        message: error
      };
    }
  };

  config.oss = {
    client: {
      accessKeyId: 'LTAICPYVAvnzD7pY',
      accessKeySecret: 'gX7WKtYuiPRsmEbXyWeXkso85Ds1jL',
      bucket: 'qianqian-server',
      endpoint: 'oss-cn-beijing.aliyuncs.com',
      timeout: '60s'
    }
  }

  config.alinode = {
    server: 'wss://agentserver.node.aliyun.com:8080',
    appid: '76213',
    secret: 'c6fb32e229f2d5dd42ab65fc43e30860e7a7995d'
  };

  // config.jwt = {
  //   secret: "RRFE666"
  // };

  config.static = {
    gzip: true
  }

  // config.redis = {
  //   client: {
  //     port: 6999,          // Redis port
  //     host: '127.0.0.1',   // Redis host
  //     password: 'auth',
  //     db: 0
  //   }
  // }

  return config;
};
